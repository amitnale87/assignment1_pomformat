package com.testclasses.pkg;

import java.io.IOException;

import org.testng.annotations.Test;

import com.baseclass.pkg.Base;
import com.pomclasses.pkg.HomePage;

public class FlightBookTest extends Base
{
	HomePage homepage;

	@Test
	public void bookingFlightExecution() throws InterruptedException, IOException
	{
		homepage = new HomePage(driver);
		homepage.bookingExecution(driver);
				
	}

}
