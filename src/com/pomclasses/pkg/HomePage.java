package com.pomclasses.pkg;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.commonUtilities.pkg.CommonOperations;

public class HomePage extends CommonOperations
{
	@FindBy(id="tab-flight-tab")
	private WebElement flightsLink;

	@FindBy(id=("flight-origin"))
	private WebElement flyingFromTextField; 

	@FindBy(id=("flight-destination"))
	private WebElement flyingToTextField;

	@FindBy(id=("aria-option-0"))
	private WebElement firstFromCity;

	@FindBy(id=("aria-option-0"))
	private WebElement firstToCity;

	@FindBy (id=("flight-departing"))
	private WebElement departDateTextField;

	@FindBy(xpath=("//div[@id='flight-departing-wrapper']/div/div/div[2]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeDeparttureDateList;

	@FindBy (id=("flight-returning"))
	private WebElement ruturnDateTextField;

	@FindBy(xpath=("//div[@id='flight-returning-wrapper']/div/div/div[3]/"
			+ "table/tbody//tr//td/button[@class != 'datepicker-cal-date disabled']"))
	private List <WebElement> activeReturnDateList;

	@FindBy(id=("search-button"))
	private WebElement searchButton;

	WebDriverWait wait=null;

	@FindBy(css="ul[class='utility-nav nav-group cf'] >li[id*='-header']>a:not([id='primary-header-cruiseLegacy'])")
	private List<WebElement> homePageLinksList;


	public HomePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		wait=new WebDriverWait(driver, 20);
	}

	public void bookingExecution(WebDriver driver) throws InterruptedException, IOException 
	{
		Thread.sleep(500);
		flightsLink.click();

		/*Select From City*/
		flyingFromTextField.click();
		flyingFromTextField.sendKeys("Pune");
		//Thread.sleep(2000);
		//firstFromCity.click();
		wait.until(ExpectedConditions.elementToBeClickable(firstFromCity)).click();


		/*Select To City*/
		flyingToTextField.click();
		flyingToTextField.sendKeys("Mumbai");
		wait.until(ExpectedConditions.elementToBeClickable(firstToCity)).click();


		/*Select Departure Date*/
		departDateTextField.click();
		activeDeparttureDateList.get(0).click();

		/*Select Return Date*/
		ruturnDateTextField.click();
		activeReturnDateList.get(2).click();

		CommonOperations.takeScreenShot(driver, ".\\Screenshots\\BeforeSearch.png");
		searchButton.click();
		CommonOperations.takeScreenShot(driver, ".\\Screenshots\\AfterSearch.png");

	}

	public void homepageLinks(WebDriver driver) throws IOException
	{
		String[] homePageLinksArr = { "Home\ncurrently selected", "Hotels", "Flights", "Packages", "Cars", 
				"Vacation Rentals", "Cruises",
				"Deals", "Activities", "Discover", "Mobile", "Rewards" };

		System.out.println("Total Links are : " + homePageLinksList.size());

		for (int i = 0; i < homePageLinksList.size(); i++) {

			//System.out.println(homePageLinksList.get(i).getText());
			//Assert.assertEquals("Link text mismatches", homePageLinksArr[i], homePageLinksList.get(i).getText());
			String pagename = homePageLinksList.get(i).getText() + ".png";
			
			homePageLinksList.get(i).click();
			System.out.println("Pagename: "+pagename);
			/*
			 * Could not call takescreenshot method becuse Home page name contains new line character
			 * So File can't be created using New line(Special character in file name)
			 * */
			//CommonOperations.takeScreenShot(driver, ".\\HomePageLinksScreenShots\\" + pagename);

		}

	}


}
