package com.commonUtilities.pkg;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CommonOperations
{
	public static void takeScreenShot(WebDriver driver, String filepath) throws IOException {
		TakesScreenshot scrShot = (TakesScreenshot) driver;
		File scrFile = scrShot.getScreenshotAs(OutputType.FILE);
		File desFile = new File(filepath);
		FileUtils.copyFile(scrFile, desFile);
	}


}
