package com.baseclass.pkg;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Base
{
	public WebDriver driver;
	String browserPath;

	@BeforeTest
	public void setUp()
	{
		browserPath = ".\\ChromeBrowserDriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", browserPath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.orbitz.com/");
	}

	@AfterTest
	public void closeBrowser()
	{
		driver.quit();
	}
	

}
